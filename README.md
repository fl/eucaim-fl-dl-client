# EUCAIM FL DL Client


## Introduction

This repository contains the `Dockerfile` and the `client.py` for the client to be used in the [EUCAIM-WP6's demonstration](https://github.com/EUCAIM/fl_demonstrator) for Federated Learning using deep learning models (aka. CNN).

## Installing the image

FEM-client assumes that the image of the tools are pre-installed in the host's machine. Follow this instructions to download the image of eucaim-fl-dl-client:

1. Download the image from the docker registry. The credentials to log in are the same username and password used to access GitLab.

```
docker login gitlab.bsc.es
docker pull registry.gitlab.bsc.es/fl/eucaim-fl-dl-client:v2.1
docker logout #optional, if you don't want to keep logged in in this machine
```

2. Once the download is finished, check that docker can find ir with the following command:

```
docker images
```

## To be run

This client is to be run with its partner server located [here](https://gitlab.bsc.es/fl/eucaim-fl-dl-server). The orchestration of the experiment is to be done using the [FLmanager](https://gitlab.bsc.es/fl/FLManager).

## Data

According to the description of of the [deep learning challenge](https://github.com/EUCAIM/demo_dl_data/), the data is to be obtained from [this KAGGLE challenge](https://www.kaggle.com/datasets/paultimothymooney/chest-xray-pneumonia) as a zip file. One on the hard drive, unzip it.

Then, each participant data site should look into the `data_ids` for the [three data-sites scenario](https://github.com/EUCAIM/demo_dl_data/tree/main/data_ids/three_dataseties_scenario) and download the four files for the site (each site being `1`, `2` or `3`):

   * `test.nrm.3_[site_num].csv`
   * `test.pnm.3_[site_num].csv`
   * `train.nrm.3_[site_num].csv`
   * `train.pnm.3_[site_num].csv`

The unzip file has created 3 folders (`test`, `train`, `val`). From the `test` folder, remove all pictures which name is not in `test.nrm.3_[site_num].csv` nor in `test.pnm.3_[site_num].csv`. From `train` folder remove all pictures which name is not in `train.nrm.3_[site_num].csv` nor in `train.pnm.3_[site_num].csv`. Folder `val` is not used.
